import { ActionProps } from "actions/actionProps";
import { testInitialState } from "store/initialState";

export const TestCreatorReducer = (
	state = testInitialState,
	action: ActionProps,
) => {
	switch (action.type) {
		case "createTest/SET_SETTINGS":
			return { ...state, config: action.payload };
		case "createTest/SAVE_TEST_QUESTIONS":
			return { ...state, questions: action.payload };
		case "createTest/SAVE_TEST_QUESTION_BY_INDEX":
			const { index, question } = action.payload;
			const questions = [
				...state.questions.slice(0, index),
				question,
				...state.questions.slice(index + 1),
			];
			return { ...state, questions };
		default:
			return state;
	}
};
