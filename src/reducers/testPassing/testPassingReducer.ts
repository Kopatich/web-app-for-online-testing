import { ActionProps } from "actions/actionProps";
import { testInitialState } from "store/initialState";

export const TestPassReducer = (
	state = testInitialState,
	action: ActionProps,
) => {
	switch (action.type) {
		case "passTest/LOAD_TEST":
			const { questions, config, _id } = action.payload;
			return { ...state, questions, config, _id };
		case "passTest/SAVE_TEST_QUESTIONS":
			return { ...state, questions: action.payload };
		case "passTest/SAVE_ANSWER_BY_INDEX":
			const stateQuestions = state.questions;
			const { index, answers } = action.payload;
			stateQuestions[index].selectedAnswerID = answers;
			return { ...state, questions: stateQuestions };
		default:
			return state;
	}
};
