import { ActionProps } from "actions/actionProps";
import { userInitialState } from "store/initialState";

export const LoginReducer = (state = userInitialState, action: ActionProps) => {
	switch (action.type) {
		case "login/LOGIN_SUCCESS":
			const { name, email, group } = action.payload;
			const status = group ? "student" : "lecturer";
			return { ...state, name, email, group, status };
		case "login/LOGOUT":
			return { ...state, name: "", email: "", group: "", status: "" };
		default:
			return state;
	}
};
