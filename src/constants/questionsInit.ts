import { Answer, Question } from "actions/testCreateActions";

export const questionsInit: Question = {
	text: "Question text",
	type: "text",
	answers: [
		{ text: "Answer 1", ID: 1, type: "text" },
		{ text: "Answer 2", ID: 2, type: "text" },
		{ text: "Answer 3", ID: 3, type: "text" },
		{ text: "Answer 4", ID: 4, type: "text" },
	] as Answer[],
	correctAnswerID: [] as number[],
	selectedAnswerID: [] as number[],
};
