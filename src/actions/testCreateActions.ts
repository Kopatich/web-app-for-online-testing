export interface TestSettings {
	name: string;
	groups: string[];
	deadline: number;
	checkRule: string;
	duration: number;
	creator: { name: string; email: string };
}

export const setTestSettings = (settings: TestSettings) => {
	return {
		type: "createTest/SET_SETTINGS",
		payload: settings,
	};
};

export interface Answer {
	text: string;
	ID: number;
	type: "text" | "image";
}

export interface Question {
	text: string;
	type: "text" | "image";
	answers: Answer[];
	correctAnswerID: number[];
	selectedAnswerID: number[];
}

export interface Test {
	questions: Question[];
	config: TestSettings;
}

export const saveTestQuestions = (test: Question[]) => {
	return {
		type: "createTest/SAVE_TEST_QUESTIONS",
		payload: test,
	};
};

export const saveTest = (test: Test) => {
	return {
		type: "createTest/SAVE_TEST",
		payload: test,
	};
};

export const saveTestQuestionByIndex = (index: number, question: Question) => {
	return {
		type: "createTest/SAVE_TEST_QUESTION_BY_INDEX",
		payload: { index, question },
	};
};
