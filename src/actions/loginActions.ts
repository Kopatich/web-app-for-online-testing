export interface User {
	name: string;
	email: string;
	group?: string;
}

export const loginSuccess = (user: User) => {
	return {
		type: "login/LOGIN_SUCCESS",
		payload: user,
	};
};

export const logout = () => {
	return {
		type: "login/LOGOUT",
		payload: null,
	};
};
