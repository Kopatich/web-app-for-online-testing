import { Question, Test } from "./testCreateActions";

export const loadTest = (test: Test) => {
	return {
		type: "passTest/LOAD_TEST",
		payload: test,
	};
};

export const savePassingTestQuestions = (questions: Question[]) => {
	return {
		type: "passTest/SAVE_TEST_QUESTIONS",
		payload: { questions },
	};
};

export const saveAnswerByIndex = (index: number, answers: number[]) => {
	return {
		type: "passTest/SAVE_ANSWER_BY_INDEX",
		payload: { index, answers },
	};
};
