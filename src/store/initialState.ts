import { Question, TestSettings } from "actions/testCreateActions";

export const initialState = {
	user: "",
	email: "",
	testToComplete: {},
	testCreate: {},
};

export const testSettingsInitialState = {
	name: "",
	groups: [""],
	deadline: new Date().getTime(),
	checkRule: "FULL_MATCH",
	duration: 0,
	creator: { name: "", email: "" },
} as TestSettings;

export const testInitialState = {
	config: testSettingsInitialState,
	questions: [] as Question[],
	_id: undefined,
};

export const userInitialState = {
	name: "",
	email: "",
	group: "",
};
