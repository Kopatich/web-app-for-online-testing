import { createStore, combineReducers } from "redux";
import { saveTestQuestions } from "actions/testCreateActions";
import { questionsInit } from "constants/questionsInit";
import { TestCreatorReducer } from "reducers/testCreating/testCreatingReducer";
import { LoginReducer } from "reducers/login/loginReducer";
import { TestPassReducer } from "reducers/testPassing/testPassingReducer";

const allReducers = combineReducers({
	test: TestCreatorReducer,
	user: LoginReducer,
	passTest: TestPassReducer,
});

export const store = createStore(allReducers);
store.dispatch(saveTestQuestions([{ ...questionsInit }]));

store.subscribe(() => {
	console.log(store.getState());
});
