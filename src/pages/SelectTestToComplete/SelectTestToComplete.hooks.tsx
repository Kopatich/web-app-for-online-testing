import { Test } from "actions/testCreateActions";
import { loadTest } from "actions/testPassingActions";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { lockWindow, setDeadline } from "tools/maximizeAndLock";

export const useSelectTestToComplete = () => {
	const { group, email } = useSelector((state: any) => state.user);
	const history = useHistory();
	const dispatch = useDispatch();

	const testsInit = [] as Test[];
	const [tests, setTests] = React.useState(testsInit);

	React.useEffect(() => {
		fetch(
			`http://localhost:9090/tests/getTests?group=${group}&active=true&student=${email}`,
		)
			.then((res) => res.json())
			.then((res) => {
				setTests(res.tests);
			});
	}, [group, email]);

	const ruleText = (rule: string) => {
		switch (rule) {
			case "FULL_MATCH":
				return "Answer is correct if all selected answers are right";
			case "PART_MATCH":
				return "You will get a percentage if you pick not all the correct answers";
			default:
				return "";
		}
	};

	const startTest = (test: Test) => {
		dispatch(loadTest(test));

		const { deadline, duration } = test.config;
		setDeadline(deadline, duration);

		lockWindow(true);
		history.push("/passTest");
	};

	return { tests, ruleText, startTest };
};
