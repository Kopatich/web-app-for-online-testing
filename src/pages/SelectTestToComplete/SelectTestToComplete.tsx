import React from "react";
import styled from "styled-components";
import { CardContent } from "@material-ui/core";
import { Test } from "actions/testCreateActions";
import { lockWindow } from "tools/maximizeAndLock";

import { useSelectTestToComplete } from "./SelectTestToComplete.hooks";

const StyledContainer = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-around;
	flex-wrap: wrap;
	position: relative;

	width: 90%;
	max-height: 85%;
	overflow-y: scroll;

	&::-webkit-scrollbar {
		width: 12px;
	}

	&::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		border-radius: 10px;
	}

	&::-webkit-scrollbar-thumb {
		border-radius: 10px;
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
	}
`;

const StyledCard = styled.div`
	height: 40%;
	width: 250px;

	display: flex;
	flex-direction: column;
	justify-content: space-between;
	outline: none;
	transition: 0.2s;
	transform: scale(0.95);
	overflow: hidden;

	&:hover {
		color: white;
		transform: scale(1);
	}
`;

const StyledCardContent = styled(CardContent)`
	background-color: rgba(219, 219, 219, 0.25);
	border: 1px solid black;
	border-radius: 20px;
	transition: 0.2s;
	cursor: pointer;
	backdrop-filter: blur(10px);

	&:hover {
		background-color: rgba(76, 76, 76, 0.5);
		backdrop-filter: blur(10px);
	}
`;

const SelectTestToComplete = () => {
	const { tests, ruleText, startTest } = useSelectTestToComplete();
	lockWindow(false);

	const cardify = (test: Test, index: number) => {
		const { config } = test;
		const { name, deadline, checkRule, duration, creator } = config;

		const options: Intl.DateTimeFormatOptions = {
			weekday: "short",
			year: "numeric",
			month: "numeric",
			day: "numeric",
			hour: "2-digit",
			minute: "2-digit",
		};
		return (
			<StyledCard
				key={index}
				onClick={() => {
					startTest(test);
				}}
			>
				<StyledCardContent>
					<h2>{name}</h2>
					<h3>
						Deadline: {new Date(deadline).toLocaleDateString("en-US", options)}
					</h3>
					<h3>Duration: {duration} min</h3>
					<h3>Creator: {creator.name}</h3>
					<span>
						<strong>Check rule: </strong>
						<p>{ruleText(checkRule)}</p>
					</span>
				</StyledCardContent>
			</StyledCard>
		);
	};

	return (
		<>
			<StyledContainer>
				{tests.map((test, i) => cardify(test, i))}
			</StyledContainer>
		</>
	);
};

export default SelectTestToComplete;
