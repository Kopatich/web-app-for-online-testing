import {
	TableContainer,
	Paper,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
} from "@material-ui/core";
import React from "react";
import styled from "styled-components";

import { Grade, useStudentGrades } from "./StudentGrades.hooks";

const StyledContainer = styled.table`
	width: 50%;
`;

const StudentGrades = () => {
	const { grades } = useStudentGrades();

	return (
		<StyledContainer>
			<TableContainer component={Paper}>
				<Table size='small' aria-label='a dense table'>
					<TableHead>
						<TableRow>
							<TableCell>Test Title</TableCell>
							<TableCell align='right'>Grade</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{grades.map((grade: Grade) => (
							<TableRow key={grade.name}>
								<TableCell component='th' scope='row'>
									{grade.name}
								</TableCell>
								<TableCell align='right'>{grade.grade}</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
		</StyledContainer>
	);
};

export default StudentGrades;
