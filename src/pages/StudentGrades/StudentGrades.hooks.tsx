import React from "react";
import { useSelector } from "react-redux";

export interface Grade {
	name: string;
	grade: number;
}

export const useStudentGrades = () => {
	const [grades, setGrades] = React.useState({
		data: [] as Grade[],
		isFetching: true,
	});
	const { email } = useSelector((state: any) => state.user);

	React.useEffect(() => {
		fetch(`http://localhost:9090/grades/getGrades?studentEmail=${email}`)
			.then((res) => res.json())
			.then((res) => {
				if (grades.isFetching) setGrades({ data: res.data, isFetching: false });
			});
	});

	return { grades: grades.data };
};
