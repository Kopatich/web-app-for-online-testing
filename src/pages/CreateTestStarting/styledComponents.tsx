import styled from "styled-components";
import { TextField } from "@material-ui/core";

export const CreateTestStartingContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	min-height: 100;
	width: 100%;
`;

export const TestnameInput = styled(TextField)`
	width: 300px;
`;

export const GroupsInput = styled(TextField)`
	width: 300px;
`;

export const DeadlinePickerContainer = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: center;
	width: 70%;
`;

export const CheckTypeSelectContainer = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: space-between;
	width: 30%;
`;

export const BottomSideContainer = styled.div`
	display: flex;
	flex-direction: column;
	height: 200px;
	width: 50vw;
	justify-content: space-around;
	align-items: center;
`;

export const StyledCheckTypeSelectContainer = styled(CheckTypeSelectContainer)`
	width: 225px;
`;
