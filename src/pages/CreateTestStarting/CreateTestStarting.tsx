import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
	Button,
	InputLabel,
	MenuItem,
	Select,
	TextField,
} from "@material-ui/core";
import { setTestSettings } from "actions/testCreateActions";
import { StyledText } from "components/StyledText/StyledText";

import {
	BottomSideContainer,
	CreateTestStartingContainer,
	GroupsInput,
	StyledCheckTypeSelectContainer,
	TestnameInput,
} from "./StyledComponents";

const CreateTestStarting = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { name, email } = useSelector((state: any) => state.user);
	const creator = { name, email };
	const [settings, setSettings] = React.useState({
		name: "",
		groups: [] as string[],
		deadline: (null as unknown) as number,
		checkRule: "FULL_MATCH",
		duration: 0,
		creator,
	});

	const handleTestNameChange = (
		event: React.ChangeEvent<{ value: string }>,
	) => {
		setSettings({ ...settings, name: event.target.value });
	};

	const handleCheckRuleChange = (
		event: React.ChangeEvent<{ value: unknown }>,
	) => {
		setSettings({ ...settings, checkRule: event.target.value as string });
	};

	const handleDurationChange = (
		event: React.ChangeEvent<{ value: string }>,
	) => {
		setSettings({
			...settings,
			duration:
				Number(event.target.value) <= 0 ? 0 : Number(event.target.value),
		});
	};

	const handleDeadlineChange = (
		event: React.ChangeEvent<{ value: unknown }>,
	) => {
		setSettings({
			...settings,
			deadline: new Date(event.target.value as string).getTime(),
		});
	};

	const handleGroupsChange = (event: React.ChangeEvent<{ value: string }>) => {
		const value = event.target.value;
		const groups = value
			.split(",")
			.filter((elem) => elem)
			.map((elem) => elem.trim());
		setSettings({ ...settings, groups: groups });
	};

	const handleSaveTestSetting = () => {
		dispatch(setTestSettings(settings));
		// EDIT: Пофиксить баг по непроверке поля для групп, для которых предназначен тест
		if (Object.values(settings).some((elem) => !elem || elem == null)) return;
		history.push("/testCreate/setQuestions");
	};

	return (
		<CreateTestStartingContainer>
			<StyledText size={72}>Create Test</StyledText>
			<TestnameInput
				error={settings.name.length === 0}
				helperText={settings.name.length === 0 && "Test must have a name"}
				required
				variant='filled'
				label='Test name'
				margin='normal'
				onChange={handleTestNameChange}
			/>
			<GroupsInput
				error={settings.groups.length === 0}
				helperText={
					settings.groups.length === 0 &&
					"Test must be assigned to a group of students"
				}
				required
				variant='filled'
				label='Test is created for:'
				multiline
				onChange={handleGroupsChange}
			/>
			{/* <TextField
				error={settings.duration === 0}
				helperText={settings.duration === 0 && "Set the duration of test"}
				required
				variant='filled'
				label='Duration in minutes'
				type='number'
				defaultValue='0'
				onChange={handleDurationChange}
			/> */}
			<>
				<label htmlFor='duration'>Duration (in mins):</label>
				<input
					required
					id='duration'
					type='number'
					onChange={handleDurationChange}
					min='0'
					step='1'
					max='86400'
				></input>
			</>
			<hr style={{ width: "90%", margin: "20px 0" }} />

			<BottomSideContainer>
				<TextField
					id='date'
					label='Deadline date'
					type='datetime-local'
					InputLabelProps={{
						shrink: true,
					}}
					onChange={handleDeadlineChange}
					error={settings.deadline === null}
					helperText={settings.deadline && "Deadline should be picked"}
					required
				/>

				<StyledCheckTypeSelectContainer>
					<InputLabel id='demo-simple-select-label'>Check type</InputLabel>
					<Select
						labelId='demo-simple-select-label'
						id='demo-simple-select'
						value={settings.checkRule}
						onChange={handleCheckRuleChange}
						required
					>
						<MenuItem value={"FULL_MATCH"}>Full match</MenuItem>
						<MenuItem value={"PART_MATCH"}>Partial match</MenuItem>
					</Select>
				</StyledCheckTypeSelectContainer>
			</BottomSideContainer>

			<Button
				variant='contained'
				color='primary'
				onClick={handleSaveTestSetting}
			>
				Continue
			</Button>
		</CreateTestStartingContainer>
	);
};

export default CreateTestStarting;
