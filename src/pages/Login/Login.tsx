import React from "react";
import styled from "styled-components";
import { Button, Input, InputAdornment } from "@material-ui/core";
import { AccountCircle } from "@material-ui/icons";
import VpnKeyRoundedIcon from "@material-ui/icons/VpnKeyRounded";
import BusinessCenterRoundedIcon from "@material-ui/icons/BusinessCenterRounded";

import { useLogin } from "./Login.hooks";

const StyledContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	width: 100%;
	height: 30%;
	color: white;
`;

const StyledUsernameInput = styled(Input)`
	margin: 10px;
	font-size: 24pt;
	color: black;
`;

const Login = () => {
	const {
		user,
		error,
		handleLoginInput,
		handlePasswordInput,
		handleLoginClick,
	} = useLogin();

	return (
		<StyledContainer>
			<StyledUsernameInput
				required
				error={error}
				onChange={handleLoginInput}
				onKeyPress={(e) => (e.code === "Enter" ? handleLoginClick() : null)}
				color={"primary"}
				value={user.user}
				startAdornment={
					<InputAdornment position='start'>
						{user.user.includes("@hse.ru") ? (
							<BusinessCenterRoundedIcon />
						) : (
							<AccountCircle />
						)}
					</InputAdornment>
				}
			/>
			<StyledUsernameInput
				required
				error={error}
				type={"password"}
				onChange={handlePasswordInput}
				onKeyPress={(e) => (e.code === "Enter" ? handleLoginClick() : null)}
				value={user.password}
				startAdornment={
					<InputAdornment position='start'>
						<VpnKeyRoundedIcon />
					</InputAdornment>
				}
			/>

			<Button
				color={"primary"}
				variant={"contained"}
				onClick={handleLoginClick}
			>
				Login
			</Button>

			{error && (user.user === "" || user.password === "") && (
				<h3 style={{ color: "red" }}>Incorrect login or password</h3>
			)}
		</StyledContainer>
	);
};

export default Login;
