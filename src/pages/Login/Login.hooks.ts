import { loginSuccess } from "actions/loginActions";
import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

export const useLogin = () => {
	const dispatch = useDispatch();
	const history = useHistory();

	const [user, setUser] = React.useState({ user: "", password: "" });
	const [error, setError] = React.useState(false);

	const handleLoginInput = (event: React.ChangeEvent<{ value: string }>) => {
		setUser({ ...user, user: event.currentTarget.value });
		if (error) setError(false);
	};

	const handlePasswordInput = (event: React.ChangeEvent<{ value: string }>) => {
		setUser({ ...user, password: event.currentTarget.value });
		if (error) setError(false);
	};

	const handleLoginClick = React.useCallback(() => {
		fetch("http://localhost:9090/users/auth", {
			method: "POST",
			body: JSON.stringify(user),
			headers: {
				"Content-Type": "application/json",
			},
		})
			.then((res) => res.json())
			.then((res) => {
				dispatch(loginSuccess(res));
				const isLecturer = res.group;
				history.push(
					isLecturer ? "/testsForStudent" : "/testCreate/setSettings",
				);
			})
			.catch(() => {
				setUser({ user: "", password: "" });
				setError(true);
			});
	}, [dispatch, history, user]);

	return {
		user,
		error,
		handleLoginInput,
		handlePasswordInput,
		handleLoginClick,
	};
};
