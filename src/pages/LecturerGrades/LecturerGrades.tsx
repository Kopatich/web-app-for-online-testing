import {
	TableContainer,
	Paper,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	MenuItem,
	Select,
} from "@material-ui/core";
import { Grade } from "pages/StudentGrades/StudentGrades.hooks";
import React from "react";
import styled from "styled-components";

import { useLecturerGrades } from "./LecturerGrades.hooks";

const StyledContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 65%;
	height: 90%;
`;

const StyledTableContainer = styled.div`
	width: 100%;
	margin-top: 15px;
`;

const LecturerGrades = () => {
	const { grades, handleSelectedTestChange } = useLecturerGrades();
	const { data }: any = grades;
	const { selectedTest } = grades;

	const tests = Object.keys(data);

	return (
		<StyledContainer>
			<Select value={selectedTest} onChange={handleSelectedTestChange} required>
				{tests.map((item: string) => (
					<MenuItem key={item} value={item}>
						{item}
					</MenuItem>
				))}
			</Select>
			<StyledTableContainer>
				<TableContainer component={Paper}>
					<Table size='small' aria-label='a dense table'>
						<TableHead>
							<TableRow>
								<TableCell>Student</TableCell>
								<TableCell align='right'>Grade</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{data[selectedTest]?.map((grade: Grade) => (
								<TableRow key={grade.name}>
									<TableCell component='th' scope='row'>
										{grade.name}
									</TableCell>
									<TableCell align='right'>{grade.grade}</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</StyledTableContainer>
		</StyledContainer>
	);
};

export default LecturerGrades;
