import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

export const useLecturerGrades = () => {
	const [grades, setGrades] = useState({
		data: {},
		isFetching: true,
		selectedTest: "",
	});
	const { email } = useSelector((state: any) => state.user);

	useEffect(() => {
		console.log('request')
		fetch(`http://localhost:9090/grades/getGradesOfLecturer?email=${email}`)
			.then((res) => res.json())
			.then((res) => {
				
				if (grades.isFetching) {
					setGrades({ ...grades, data: res.data, isFetching: false });
				}
			});
	});

	const handleSelectedTestChange = (
		event: React.ChangeEvent<{ value: unknown }>,
	) => {
		setGrades({ ...grades, selectedTest: event.target.value as string });
	};

	return { grades, handleSelectedTestChange };
};
