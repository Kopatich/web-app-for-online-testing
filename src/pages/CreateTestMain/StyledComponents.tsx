import styled from "styled-components";

export const StyledContainer = styled.div`
	display: flex;
	flex-direction: row;
	align-items: flex-start;
	height: 100vh;
	width: 100vw;
`;

export const TestPanelContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	align-items: center;
	background-color: #444;
	flex: 1;
	height: 100%;
`;

export const TilesButtonContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-around;
	max-height: 50%;
	width: 100%;
`;

export const TilesPanel = styled.div`
	max-height: 75%;
	max-width: 76%;
	min-width: 76%;
	overflow-y: scroll;

	margin-left: -5px;

	display: flex;
	flex-direction: row;
	justify-content: stretch;
	align-items: space-between;
	flex-wrap: wrap;

	&::-webkit-scrollbar {
		width: 12px;
	}

	&::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		border-radius: 10px;
	}

	&::-webkit-scrollbar-thumb {
		border-radius: 10px;
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
	}
`;

export const ButtonPanel = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-around;
	height: auto;
	width: 90%;

	margin-top: 10px;
`;

export const QuestionContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	align-items: flex-start;

	margin: 0 15px;
	height: 100%;
	flex: 3;
`;
