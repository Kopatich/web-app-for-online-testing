import React from "react";
import TestPanelCreateContainer from "components/TestPanelCreateContainer/TestPanelCreateContainer";
import QuestionPanel from "components/QuestionPanel/QuestionPanel";

import {
	StyledContainer,
	TestPanelContainer,
	QuestionContainer,
} from "./StyledComponents";
import { useCreateTestMain } from "./CreateTestMain.hooks";

interface TestMain {
	forStudents?: boolean;
}

const TestMain = ({ forStudents }: TestMain) => {
	const {
		focusedQuestion,
		setfocusedQuestion,
		handleAddQuestionClick,
		handleDeleteQuestionClick,
		questions,
	} = useCreateTestMain(forStudents);

	return (
		<StyledContainer>
			<TestPanelContainer>
				<TestPanelCreateContainer
					questions={questions}
					focusedQuestion={focusedQuestion}
					setfocusedQuestion={setfocusedQuestion}
					handleAddQuestionClick={handleAddQuestionClick}
					handleDeleteQuestionClick={handleDeleteQuestionClick}
					forStudents={forStudents}
				/>
			</TestPanelContainer>
			<QuestionContainer>
				<QuestionPanel
					questionNumber={focusedQuestion}
					forStudents={forStudents}
				/>
			</QuestionContainer>
		</StyledContainer>
	);
};

export default TestMain;
