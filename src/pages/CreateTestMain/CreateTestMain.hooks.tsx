import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Answer, Question, saveTestQuestions } from "actions/testCreateActions";
import { savePassingTestQuestions } from "actions/testPassingActions";

export const useCreateTestMain = (forStudents: boolean | undefined) => {
	const dispatch = useDispatch();
	const [focusedQuestion, setfocusedQuestion] = useState(0);

	const questions = useSelector((state: any) => {
		return !forStudents ? state.test.questions : state.passTest.questions;
	});

	const setQuestions = (newQuestions: Question[]) => {
		!forStudents
			? dispatch(saveTestQuestions(newQuestions))
			: dispatch(savePassingTestQuestions(newQuestions));
	};

	const handleAddQuestionClick = () => {
		const questionsInit: Question = {
			text: "Question text",
			type: "text",
			answers: [
				{ text: "Answer 1", ID: 1, type: "text" },
				{ text: "Answer 2", ID: 2, type: "text" },
				{ text: "Answer 3", ID: 3, type: "text" },
				{ text: "Answer 4", ID: 4, type: "text" },
			] as Answer[],
			correctAnswerID: [] as number[],
			selectedAnswerID: [] as number[],
		};
		const newOne = { ...questionsInit };
		questions.push(newOne);
		setQuestions(questions);

		setfocusedQuestion(focusedQuestion + 1);
	};

	const handleDeleteQuestionClick = () => {
		switch (focusedQuestion) {
			case questions.length - 1:
				setfocusedQuestion(questions.length - 2);
				break;
			case 0:
				setfocusedQuestion(0);
				break;
			default:
				setfocusedQuestion(focusedQuestion);
				break;
		}

		questions.splice(focusedQuestion, 1);
		setQuestions([...questions]);
	};

	return {
		focusedQuestion,
		setfocusedQuestion,
		handleAddQuestionClick,
		handleDeleteQuestionClick,
		questions,
	};
};
