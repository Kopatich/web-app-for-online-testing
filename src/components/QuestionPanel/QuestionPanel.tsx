import React from "react";
import QuestionBox from "components/QuestionBox/QuestionBox";
import AnswersPanel from "components/AnswersPanel/AnswersPanel";
import { useDispatch, useSelector } from "react-redux";
import { Tooltip, Button } from "@material-ui/core";
import { Question, saveTestQuestionByIndex } from "actions/testCreateActions";

interface QuestionPanelProps {
	questionNumber: number;
	forStudents?: boolean;
}

const QuestionPanel = ({ questionNumber, forStudents }: QuestionPanelProps) => {
	const dispatch = useDispatch();
	const activeQuestion: Question = useSelector((state: any) => {
		return !forStudents
			? state.test.questions[questionNumber]
			: state.passTest.questions[questionNumber];
	});

	return (
		<>
			<QuestionBox questionNumber={questionNumber} forStudents={forStudents} />
			<AnswersPanel
				questionNumber={questionNumber}
				forStudents={forStudents}
			></AnswersPanel>
			{!forStudents && (
				<Tooltip title={"Add answer to the question"}>
					<Button
						variant='contained'
						color='primary'
						disabled={activeQuestion.answers.length >= 6}
						onClick={() => {
							const { answers } = activeQuestion;
							const ID = answers[answers.length - 1].ID;
							activeQuestion.answers.push({
								text: "",
								ID: ID + 1,
								type: "text",
							});
							dispatch(saveTestQuestionByIndex(questionNumber, activeQuestion));
						}}
					>
						Add answer
					</Button>
				</Tooltip>
			)}
		</>
	);
};

export default QuestionPanel;
