import { saveTestQuestionByIndex } from "actions/testCreateActions";
import TestButton from "components/TestButton/TestButton";
import { useTestButton } from "components/TestButton/TestButton.hooks";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

interface QuestionBoxProps {
	questionNumber: number;
	forStudents?: boolean;
}

const StyledQuestionBox = styled.textarea`
	font-size: 36pt;
	font-family: Roboto;

	width: 90%;
	height: 35%;

	resize: none;
	border-radius: 10px;
	padding: 0 5px;
`;

const StyledImageContainer = styled.div`
	width: 90%;
	height: 35%;
	overflow-y: scroll;
`;

const StyledButtonsContainer = styled.div`
	display: flex;
	flex-direction: row;
	width: auto;
`;

const QuestionBox = ({ questionNumber, forStudents }: QuestionBoxProps) => {
	const dispatch = useDispatch();

	const activeQuestion = useSelector((state: any) => {
		return !forStudents
			? state.test.questions[questionNumber]
			: state.passTest.questions[questionNumber];
	});
	const { type, text } = React.useMemo(() => activeQuestion, [activeQuestion]);

	const handleQuestionTextChange = (
		event: React.ChangeEvent<{ value: string }>,
	) => {
		const text = event.currentTarget.value;
		dispatch(
			saveTestQuestionByIndex(questionNumber, { ...activeQuestion, text }),
		);
	};

	const handleQuestionTextChangeAsImage = React.useCallback(
		(text: string, type?: string) => {
			dispatch(
				saveTestQuestionByIndex(questionNumber, {
					...activeQuestion,
					text,
					type,
				}),
			);
		},
		[activeQuestion, dispatch, questionNumber],
	);

	const { convertImage, clearData } = useTestButton(
		handleQuestionTextChangeAsImage,
	);

	return (
		<>
			{type === "text" ? (
				<StyledQuestionBox
					required
					rows={5}
					placeholder={"Enter the question"}
					onChange={handleQuestionTextChange}
					value={text}
					readOnly={forStudents}
				></StyledQuestionBox>
			) : (
				<StyledImageContainer>
					<img src={text} />
				</StyledImageContainer>
			)}
			{!forStudents ? (
				<StyledButtonsContainer>
					<TestButton
						onChange={(e) => {
							convertImage(e.currentTarget);
							handleQuestionTextChangeAsImage(text, "image");
						}}
					/>
					<TestButton
						shouldClear
						onClick={() => {
							clearData();
							handleQuestionTextChangeAsImage("", "text");
						}}
					/>
				</StyledButtonsContainer>
			) : null}
		</>
	);
};

export default QuestionBox;
