import React from "react";
import { Question } from "actions/testCreateActions";
import {
	TilesButtonContainer,
	TilesPanel,
} from "pages/CreateTestMain/StyledComponents";
import { useTestPanelCreateContainer } from "./TestPanelCreateContainer.hooks";

export interface TestPanelCreateContainerProps {
	questions: Question[];
	focusedQuestion: number;
	setfocusedQuestion: (value: React.SetStateAction<any>) => void;
	forStudents?: boolean;
	handleAddQuestionClick: () => void;
	handleDeleteQuestionClick: () => void;
}

const TestPanelCreateContainer = ({
	questions,
	focusedQuestion,
	setfocusedQuestion,
	handleAddQuestionClick,
	handleDeleteQuestionClick,
	forStudents,
}: TestPanelCreateContainerProps) => {
	const {
		timerDOM,
		questionDOM,
		lecturerButtonsDOM,
		studentsButtonDOM,
	} = useTestPanelCreateContainer({
		questions,
		focusedQuestion,
		setfocusedQuestion,
		handleAddQuestionClick,
		handleDeleteQuestionClick,
		forStudents,
	});

	return (
		<TilesButtonContainer>
			{timerDOM}
			<h2 style={{ color: "white" }}>Questions:</h2>
			<TilesPanel>{questionDOM}</TilesPanel>
			{lecturerButtonsDOM}
			{studentsButtonDOM}
		</TilesButtonContainer>
	);
};

export default TestPanelCreateContainer;
