import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { Question } from "actions/testCreateActions";
import { countTimeLeft, lockWindow } from "tools/maximizeAndLock";
import QuestionTile from "components/Tile/Tile";
import Timer from "components/Timer/Timer";
import { TestPanelCreateContainerProps } from "./TestPanelCreateContainer";
import { Tooltip, Button } from "@material-ui/core";
import { ButtonPanel } from "pages/CreateTestMain/StyledComponents";

export const useTestPanelCreateContainer = ({
	questions,
	focusedQuestion,
	setfocusedQuestion,
	handleAddQuestionClick,
	handleDeleteQuestionClick,
	forStudents,
}: TestPanelCreateContainerProps) => {
	const test = useSelector((state: any) =>
		forStudents ? state.passTest : state.test,
	);
	const user = useSelector((state: any) => state.user);
	const history = useHistory();
	const { deadline, duration } = test.config;

	const handleSaveTestClick = () => {
		console.log("click", test);
		fetch("http://localhost:9090/tests/saveTest", {
			method: "POST",
			body: JSON.stringify(test),
			headers: {
				"Content-Type": "application/json",
			},
		}).then((r) => {
			console.log("save", test);
			history.push(forStudents ? "/testsForStudent" : "/grades/lecturerGrades");
		});
	};

	const handleSubmitTestClick = () => {
		const testToSend = {
			studentEmail: user.email,
			studentName: user.name,
			testID: test._id,
			selectedAnswers: test.questions.map((q: Question) => q.selectedAnswerID),
		};

		fetch("http://localhost:9090/grades/saveAnswer", {
			method: "POST",
			body: JSON.stringify(testToSend),
			headers: {
				"Content-Type": "application/json",
			},
		}).then(() => {
			history.push("/testsForStudent");
			lockWindow(false);
		});
	};

	const timerDOM = forStudents && (
		<Timer
			handleSubmitTestClick={handleSubmitTestClick}
			time={countTimeLeft(deadline, duration)}
		/>
	);

	const questionDOM = questions.map((_, i) => (
		<QuestionTile
			focused={i === focusedQuestion}
			key={i}
			onClick={() => {
				setfocusedQuestion(i);
			}}
		>
			{i + 1}
		</QuestionTile>
	));

	const lecturerButtonsDOM = !forStudents && (
		<ButtonPanel>
			<Tooltip title={"Save this question and create a new one"}>
				<Button
					variant='contained'
					color='primary'
					onClick={handleAddQuestionClick}
				>
					Add
				</Button>
			</Tooltip>
			<Tooltip title={"Delete this question"}>
				{questions.length === 1 ? (
					<span>
						<Button
							variant='contained'
							color='secondary'
							disabled={questions.length === 1}
							onClick={handleDeleteQuestionClick}
						>
							Delete
						</Button>
					</span>
				) : (
					<Button
						variant='contained'
						color='secondary'
						disabled={questions.length === 1}
						onClick={handleDeleteQuestionClick}
					>
						Delete
					</Button>
				)}
			</Tooltip>
			<Tooltip title={"Save the test and send it to the server"}>
				<Button variant='contained' onClick={handleSaveTestClick}>
					Save & send
				</Button>
			</Tooltip>
		</ButtonPanel>
	);

	const studentsButtonDOM = forStudents && (
		<ButtonPanel>
			<Tooltip title={"Submit the test"}>
				<Button variant='contained' onClick={handleSubmitTestClick}>
					Submit test
				</Button>
			</Tooltip>
		</ButtonPanel>
	);

	console.log(forStudents);

	return {
		handleSaveTestClick,
		handleSubmitTestClick,
		timerDOM,
		questionDOM,
		lecturerButtonsDOM,
		studentsButtonDOM,
	};
};
