import React from "react";
import styled from "styled-components";

interface TextContainerProps {
	size: number;
	margin?: string;
	children?: React.ReactNode;
}

const TextContainer = styled.div<TextContainerProps>`
	font-size: ${({ size }) => size}pt;
	font-family: Roboto;
	font-weight: 400;

	margin: ${({ margin }) => margin && "0"};
`;

export const StyledText = ({ size, children }: TextContainerProps) => {
	return <TextContainer size={size}>{children}</TextContainer>;
};

export default StyledText;
