import React, { useEffect, useState } from "react";
import styled from "styled-components";

const TimerContainer = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: space-around;
	width: 100%;
	color: white;
`;

interface TimerProps {
	time: number;
	handleSubmitTestClick: () => void;
}

const Timer = ({ time, handleSubmitTestClick }: TimerProps) => {
	const [timer, setTimer] = useState(time);

	useEffect(() => {
		setInterval(() => {
			setTimer((prev) => prev - 1000);
		}, 1000);
	}, []);

	const options: Intl.DateTimeFormatOptions = {
		hour: "2-digit",
		minute: "2-digit",
		second: "2-digit",
		timeZone: "Iceland",
		hour12: false,
	};

	const formatter = new Intl.DateTimeFormat("en-GB", options);

	useEffect(() => {
		if (timer === 0) handleSubmitTestClick();
	});

	return (
		<TimerContainer>
			<h2>Time remaining: </h2>
			<h2>{formatter.format(timer)}</h2>
		</TimerContainer>
	);
};

export default Timer;
