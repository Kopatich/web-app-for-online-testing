import { Tooltip, IconButton } from "@material-ui/core";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
	Answer as AnswerType,
	Question,
	saveTestQuestionByIndex,
} from "actions/testCreateActions";
import TestButton from "components/TestButton/TestButton";
import { useTestButton } from "components/TestButton/TestButton.hooks";
import DeleteIcon from "@material-ui/icons/Delete";

const StyledContainer = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	width: 70%;
`;

const AnswerTextContainer = styled.textarea`
	resize: none;
	font-style: Roboto;
	font-size: 16pt;
	width: 100%;

	margin-left: 15px;
`;

const StyledImageContainer = styled.div`
	width: 100%;
	height: 90px;
	overflow-y: scroll;

	& img {
		width: 100%;
	}
`;

interface AnswerProps {
	answer: AnswerType;
	questionNumber: number;
	forStudents?: boolean;
}

const Answer = ({ answer, questionNumber, forStudents }: AnswerProps) => {
	const dispatch = useDispatch();
	const activeQuestion: Question = useSelector((state: any) => {
		return !forStudents
			? state.test.questions[questionNumber]
			: state.passTest.questions[questionNumber];
	});

	const text = activeQuestion.answers.filter(
		(a: AnswerType) => a.ID === answer.ID,
	)[0].text;
	let textInput: HTMLTextAreaElement | null = null;

	const setFocus = React.useCallback(() => textInput?.focus(), [textInput]);

	const handleAnswerTextChange = (
		event: React.ChangeEvent<{ value: string }>,
	) => {
		const text = event.currentTarget.value;
		const question = { ...activeQuestion };
		const { answers } = question;

		const newAnswers = answers.map((a: AnswerType) => {
			if (a.ID === answer.ID) a.text = text;
			return a;
		});
		const modifiedQuestion = { ...question, answers: newAnswers };
		setFocus();
		dispatch(saveTestQuestionByIndex(questionNumber, modifiedQuestion));
	};

	const handleAnswerTextChangeAsImage = React.useCallback(
		(text: string, type: "text" | "image") => {
			const question = { ...activeQuestion };
			const { answers } = question;

			const newAnswers = answers.map((a: AnswerType) => {
				if (a.ID === answer.ID) {
					a.text = text;
					a.type = type;
				}
				return a;
			});
			const modifiedQuestion = { ...question, answers: newAnswers };
			setFocus();
			dispatch(saveTestQuestionByIndex(questionNumber, modifiedQuestion));
		},
		[activeQuestion, dispatch, questionNumber, setFocus, answer],
	);

	const { convertImage, clearData } = useTestButton(
		handleAnswerTextChangeAsImage,
	);

	return (
		<StyledContainer>
			{answer.type === "text" ? (
				<AnswerTextContainer
					readOnly={forStudents}
					cols={60}
					rows={2}
					value={text}
					ref={(area: HTMLTextAreaElement) => (textInput = area)}
					onChange={handleAnswerTextChange}
				></AnswerTextContainer>
			) : (
				<StyledImageContainer>
					<img src={answer.text}></img>
				</StyledImageContainer>
			)}
			{!forStudents && (
				<>
					<Tooltip title={"Delete answer from the question"}>
						<IconButton
							component='span'
							color='secondary'
							onClick={() => {
								activeQuestion.answers = activeQuestion.answers.filter(
									(a: AnswerType) => a.ID !== answer.ID,
								);
								activeQuestion.correctAnswerID = activeQuestion.correctAnswerID.filter(
									(id: number) => answer.ID !== id,
								);
								dispatch(
									saveTestQuestionByIndex(questionNumber, activeQuestion),
								);
							}}
						>
							<DeleteIcon fontSize='small' />
						</IconButton>
					</Tooltip>
					<TestButton
						id={answer}
						onChange={(e) => {
							convertImage(e.target);
						}}
					/>
					<TestButton shouldClear onClick={clearData} />
				</>
			)}
		</StyledContainer>
	);
};

export default Answer;
