import React from "react";
import { NavLink } from "react-router-dom";
import "./style.css";

export interface NavbarButtonProps {
	icon: string;
	text?: string;
	linkTo: string;
}

export const NavbarButton = ({ icon, text, linkTo }: NavbarButtonProps) => (
	<NavLink to={linkTo} className='MenuButton'>
		<img src={icon} alt='' />
		<span>{text}</span>
	</NavLink>
);

export default NavbarButton;
