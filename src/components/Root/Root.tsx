import React from "react";
import {
	Route,
	BrowserRouter as Router,
	Switch,
	Redirect,
} from "react-router-dom";
import { Provider } from "react-redux";
import CreateTestStarting from "pages/CreateTestStarting/CreateTestStarting";
import AppLayout from "components/AppLayout/AppLayout";
import TestMain from "pages/CreateTestMain/CreateTestMain";
import Login from "pages/Login/Login";
import SelectTestToComplete from "pages/SelectTestToComplete/SelectTestToComplete";
import StudentGrades from "pages/StudentGrades/StudentGrades";
import { store } from "store/store";
import LecturerGrades from "pages/LecturerGrades/LecturerGrades";

export const Root = () => {
	return (
		<Provider store={store}>
			<Router>
				<Switch>
					<AppLayout>
						<Route exact path='/'>
							<Redirect to='/login' />
						</Route>
						<Route path='/login' exact>
							<Login />
						</Route>
						<Route path='/testsForStudent' exact>
							<SelectTestToComplete />
						</Route>
						<Route path='/passTest' exact>
							<TestMain forStudents />
						</Route>
						<Route path='/testCreate/setSettings' exact>
							<CreateTestStarting />
						</Route>
						<Route path='/testCreate/setQuestions' exact>
							<TestMain />
						</Route>
						<Route path='/grades/studentGrades' exact>
							<StudentGrades />
						</Route>
						<Route path='/grades/lecturerGrades' exact>
							<LecturerGrades />
						</Route>
					</AppLayout>
				</Switch>
			</Router>
		</Provider>
	);
};
