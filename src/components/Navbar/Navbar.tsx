import React from "react";
import styled from "styled-components";
import {
	NavbarButton,
	NavbarButtonProps,
} from "components/NavbarButton/NavbarButton";

export interface NavbarProps {
	isHidden: boolean;
	buttons: any;
}

const NavbarWrapper = styled.div`
	width: 50px;
	height: auto;
`;

export const Navbar = ({ isHidden, buttons }: NavbarProps) => {
	const buttonsComponents = buttons.map(
		({ icon, text, linkTo }: NavbarButtonProps) => (
			<NavbarButton icon={icon} text={text} key={text} linkTo={linkTo} />
		),
	);

	return <NavbarWrapper>{isHidden ? null : buttonsComponents}</NavbarWrapper>;
};
