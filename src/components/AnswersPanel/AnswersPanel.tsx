import {
	Answer as AnswerType,
	saveTestQuestionByIndex,
} from "actions/testCreateActions";
import { saveAnswerByIndex } from "actions/testPassingActions";
import Answer from "components/Answer/Answer";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

interface AnswersPanelProps {
	questionNumber: number;
	forStudents?: boolean;
}

const StyledContainer = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
`;

const AnswersPanel = ({ questionNumber, forStudents }: AnswersPanelProps) => {
	const activeQuestion = useSelector((state: any) =>
		forStudents
			? state.passTest.questions[questionNumber]
			: state.test.questions[questionNumber],
	);
	const dispatch = useDispatch();

	const { answers } = activeQuestion;

	const selected = React.useMemo(() => activeQuestion.selectedAnswerID, [
		activeQuestion.selectedAnswerID,
	]);
	const correct = React.useMemo(() => activeQuestion.correctAnswerID, [
		activeQuestion.correctAnswerID,
	]);

	const handleAnswerChecked = React.useCallback(
		(event: React.ChangeEvent<HTMLInputElement>, answer: AnswerType) => {
			const isChecked = event.currentTarget.checked;
			let { correctAnswerID, selectedAnswerID } = activeQuestion;

			if (isChecked) {
				!forStudents
					? correctAnswerID.push(answer.ID)
					: selectedAnswerID.push(answer.ID);
			} else {
				!forStudents
					? (correctAnswerID = correctAnswerID.filter(
							(id: number) => id !== answer.ID,
					  ))
					: (selectedAnswerID = selectedAnswerID.filter(
							(id: number) => id !== answer.ID,
					  ));
			}

			const modifiedQuestion = {
				...activeQuestion,
				correctAnswerID,
			};
			!forStudents
				? dispatch(saveTestQuestionByIndex(questionNumber, modifiedQuestion))
				: dispatch(saveAnswerByIndex(questionNumber, selectedAnswerID));
		},
		[activeQuestion, questionNumber, dispatch, forStudents],
	);

	return (
		<>
			{answers.map((answer: AnswerType) => {
				const checked = forStudents
					? selected.includes(answer.ID)
					: correct.includes(answer.ID);

				return (
					<StyledContainer key={answer.ID}>
						<input
							defaultChecked={forStudents ? checked : undefined}
							checked={!forStudents ? checked : undefined}
							type={"checkbox"}
							onChange={(e) => handleAnswerChecked(e, answer)}
							key={answer.text + questionNumber + answer.type}
						></input>
						<Answer
							key={questionNumber}
							answer={answer}
							questionNumber={questionNumber}
							forStudents={forStudents}
						/>
					</StyledContainer>
				);
			})}
		</>
	);
};

export default AnswersPanel;
