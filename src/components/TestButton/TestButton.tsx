import React from "react";

import BackspaceIcon from "@material-ui/icons/Backspace";
import ImageIcon from "@material-ui/icons/Image";
import IconButton from "@material-ui/core/IconButton";
import { Tooltip } from "@material-ui/core";
import { Answer } from "actions/testCreateActions";

export interface TestButtonProps {
	shouldClear?: boolean;
	onClick?: () => void;
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
	id?: Answer;
}

const TestButton = ({
	onClick,
	shouldClear,
	onChange,
	id,
}: TestButtonProps) => {
	return (
		<>
			{shouldClear ? (
				<Tooltip title='Clear'>
					<IconButton onClick={onClick} color='primary' component='span'>
						<BackspaceIcon fontSize='small' />
					</IconButton>
				</Tooltip>
			) : (
				<>
					<input
						type='file'
						id={"upload-button" + id}
						onChange={onChange}
						accept='image/*'
						style={{ display: "none" }}
					></input>
					<Tooltip title='Set image as a question'>
						<label htmlFor={"upload-button" + id}>
							<IconButton
								color='primary'
								aria-label='upload picture'
								component='span'
							>
								<ImageIcon />
							</IconButton>
						</label>
					</Tooltip>
				</>
			)}
		</>
	);
};

export default TestButton;
