import { convertImageToBase64 } from "tools/convertToBase64";

export const useTestButton = (
	setState: (text: string, type: "text" | "image") => any,
) => {
	const convertImage = (element: any) => {
		convertImageToBase64(element, setState);
	};

	const clearData = () => convertImageToBase64("", setState, true);

	return { convertImage, clearData };
};
