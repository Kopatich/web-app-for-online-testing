import React from "react";
import styled from "styled-components";
// @ts-ignore
import logo from "assets/bg1.png";
import { Drawer, List, ListItem, ListItemText } from "@material-ui/core";
import { useLocation } from "react-router";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

interface AppLayourProps {
	visible?: boolean;
	children?: React.ReactNode;
}

const StyledContainer = styled.div<AppLayourProps>`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100vw;
	height: 100vh;
	position: relative;
	background-image: url(${logo});
`;

const AppLayout = ({ children }: AppLayourProps) => {
	const location = useLocation();
	const history = useHistory();

	const { status } = useSelector((state: any) => state.user);

	const studentLocations = ["/testsForStudent", "/grades/studentGrades"];
	const lecturerLocations = [
		"/testCreate/setSettings",
		"/grades/lecturerGrades",
	];

	return (
		<StyledContainer>
			{location.pathname !== "/passTest" &&
				location.pathname !== "/testCreate/setQuestions" && (
					<Drawer variant='permanent' anchor='left'>
						<List>
							{status === "student" &&
								["Tests", "Grades"].map((text, index) => (
									<ListItem
										button
										key={text}
										onClick={() => history.push(studentLocations[index])}
									>
										<ListItemText primary={text} />
									</ListItem>
								))}
							{status === "lecturer" &&
								["Create test", "Grades"].map((text, index) => (
									<ListItem
										button
										key={text}
										onClick={() => history.push(lecturerLocations[index])}
									>
										<ListItemText primary={text} />
									</ListItem>
								))}
						</List>
					</Drawer>
				)}
			{children}
		</StyledContainer>
	);
};

export default AppLayout;
