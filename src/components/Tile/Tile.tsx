import React from "react";
import styled from "styled-components";

interface TileProps {
	children?: React.ReactNode;
	onClick?: () => void;
	focused: boolean;
}

const StyledTileContainer = styled.div<TileProps>`
	margin: 5px;
	color: black;
	font-family: Roboto;
	font-weight: bold;
	cursor: pointer;

	width: 24pt;
	height: 24pt;

	display: flex;
	justify-content: center;
	align-items: center;

	background-color: ${({ focused }) => (focused ? "#fff" : "#ccc")};
	border: 1px solid ${({ focused }) => (focused ? "red" : "black")};
`;

const QuestionTile = ({ focused, children, onClick }: TileProps) => {
	return (
		<StyledTileContainer onClick={onClick} focused={focused}>
			{children}
		</StyledTileContainer>
	);
};

export default QuestionTile;
