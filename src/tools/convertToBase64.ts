export const convertImageToBase64 = (
	element: any,
	setter: (text: string, type: "text" | "image") => any,
	clear?: boolean,
): string => {
	try {
		const file = element.files[0];
		const reader = new FileReader();
		let encode = "";

		reader.onload = function () {
			encode = reader.result?.toString() as string;
			clear ? setter("", "text") : setter(encode, "image");
		};
		reader.onerror = function () {
			return;
		};
		reader.readAsDataURL(file);

		return encode;
	} catch (e) {
		setter("", "text");
		return "";
	}
};
