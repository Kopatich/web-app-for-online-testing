const { ipcRenderer } = (window as any).require("electron");

export const countTimeLeft = (deadline: number, minutes: number) => {
	return Math.min(deadline - Date.now(), minutes * 60 * 1000);
};

export const lockWindow = (lock: boolean) => {
	lock
		? ipcRenderer.send("activateScreenLock")
		: ipcRenderer.send("unlockScreen");
};

export const setDeadline = (deadline: number, minutes: number) => {
	const duration = countTimeLeft(deadline, minutes);
	ipcRenderer.send("setDeadline", duration);
};

export const clearDeadline = () => {
	ipcRenderer.send("clearDeadline");
};
