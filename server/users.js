const express = require("express");
const StudentModel = require("./models/studentModel");
const LecturerModel = require("./models/lecturerModel");

const router = express.Router();

router.post("/auth", async (req, res) => {
	const { body } = req;
	const { user, password } = body;
	const selector = user.includes("@")
		? {
				password,
				email: user,
		  }
		: {
				password,
				name: user,
		  };

	const isStudent = await StudentModel.find(selector);
	const isLecturer = await LecturerModel.find(selector);
	const [foundUser] = isStudent.concat(isLecturer);

	try {
		if (!foundUser) throw new Error("Incorrect username or password");
		const { name, email, group } = foundUser;
		res.json({ name, email, group });
	} catch (err) {
		console.log(err.message);
		res.sendStatus(404);
	}
});

module.exports = router;
