const express = require("express");
var bodyParser = require("body-parser");
var boolParser = require("express-query-boolean");
var cors = require("cors");

const corsOptions = {
	origin: "http://localhost:3000",
	optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const app = express();
app.use(cors(corsOptions));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.use(boolParser());

const testsRouter = require("./tests");
const usersRouter = require("./users");
const gradesRouter = require("./grades");

app.use("/tests", testsRouter);
app.use("/users", usersRouter);
app.use("/grades", gradesRouter);

const port = 9090;

app.listen(port, function () {
	console.log(`Server is active on port ${port}`);
});
