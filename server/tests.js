const express = require("express");
const { TestModel } = require("./models/testModel");
const GradeModel = require("./models/gradeModel");

const router = express.Router();

router.post("/saveTest", (req, res) => {
	const model = new TestModel(req.body);
	model
		.save()
		.then((doc) => {
			if (!doc || doc.length === 0) {
				res.status(500);
			} else {
				res.send("OK!");
			}
		})
		.catch((err) => {
			console.log(err.message);
			res.status(500).send(err);
		});
});

router.delete("/dropTests", (req, res) => {
	TestModel.deleteMany({}).then((doc) => {
		res.json(doc);
		console.log("DATABASE TESTS WAS DROPPED");
	});
});

router.get("/getTests", (req, res) => {
	const { group, active, student } = req.query;
	const deadline = active ? Date.now() : 0;
	const selector = {
		"config.groups": {
			$in: [group],
		},
		"config.deadline": {
			$gt: deadline,
		},
	};

	GradeModel.find({ studentEmail: student }, (err, docs) => {
		const testIDs = docs.map((d) => d.testID);
		TestModel.find({ ...selector, _id: { $nin: testIDs } })
			.then((doc) => {
				res.json({ tests: doc });
			})
			.catch((err) => console.log(err));
	});
});

router.get("/getTestByID", (req, res) => {
	const { _id } = req.query;
	const selector = {
		_id,
	};

	TestModel.find(selector)
		.then((doc) => {
			res.json({
				test: doc[0],
			});
		})
		.catch((err) => console.log(err));
});

module.exports = router;
