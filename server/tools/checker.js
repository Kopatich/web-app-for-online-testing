const checkTest = (selectedAnswers, correctAnswers, checkRule) => {
	let correct = 0,
		all = 0;

	selectedAnswers.forEach((answersArray, questionNumber) => {
		const correctAnswer = correctAnswers[questionNumber];
		if (checkRule === "PART_MATCH") {
			answersArray.forEach((a) => {
				correct += correctAnswer.includes(a) ? 1 : 0;
				all += 1;
			});
		} else {
			sA = sortedIDs(answersArray);
			cA = sortedIDs(correctAnswer);

			correct += Number(
				sA.length === cA.length &&
					sA.every(function (element, index) {
						return element === cA[index];
					}),
			);
			all += 1;
		}
	});

	return (correct / all).toFixed(2);
};

const sortedIDs = (arr) => {
	const s = arr.sort((a, b) => (a > b ? 1 : a < b ? -1 : 0));
	return s;
};

module.exports = { checkTest };
