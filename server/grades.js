const express = require("express");
const GradeModel = require("./models/gradeModel");
const { TestModel } = require("./models/testModel");
const fetch = require("node-fetch");

const { checkTest } = require("./tools/checker");

const router = express.Router();

router.get("/getGrades", (req, res) => {
	const { studentEmail } = req.query;
	const selector = { studentEmail };
	GradeModel.find(selector).then((obj) => {
		const testIDs = obj.map((d) => d.testID);

		TestModel.find({ _id: { $in: testIDs } }, (err, docs) => {
			const data = [];
			const testNames = docs.map((d) => d.config.name);
			const grades = obj.map((o) => o.grade);

			testNames.forEach((name, index) => {
				const unit = { name: name, grade: grades[index] };
				data.push(unit);
			});
			res.json({ data });
		});
	});
});

router.get("/getGradesOfLecturer", (req, res) => {
	const { email } = req.query;
	const selector = { "config.creator.email": email };

	TestModel.find(selector).then((doc) => {
		const arr = {};
		doc.forEach((d) => {
			GradeModel.find({ testID: d._id }, (err, docs) => {
				for (let gradeArr of docs) {
					const { studentName, grade } = gradeArr;
					if (!arr[d.config.name]) arr[d.config.name] = [];
					arr[d.config.name].push({ name: studentName, grade });
				}
				Object.keys(arr).map((test) =>
					arr[test].sort((a, b) =>
						a.name < b.name ? 1 : a.name > b.name ? -1 : 0,
					),
				);
				try {
				res.json({ data: arr });
				console.log(arr); 
			} catch (e) {}
			})
			.catch(e => {});
		});
	});
});

router.post("/saveAnswer", async (req, res) => {
	const { testID, selectedAnswers } = req.body;

	const requestForTest = await fetch(
		`http://localhost:9090/tests/getTestByID?_id=${testID}`,
	);
	const receivedTest = await requestForTest.json();
	const { questions, config } = receivedTest.test;
	const correctAnswers = questions.map((q) => q.correctAnswerID);
	const { checkRule } = config;

	const grade = checkTest(selectedAnswers, correctAnswers, checkRule);

	const model = new GradeModel({ ...req.body, grade });

	model
		.save()
		.then((doc) => {
			if (!doc || doc.length === 0) {
				return res.status(500).send(doc);
			}
			res.status(201).send(doc);
		})
		.catch((err) => {
			console.log(err.message);
			res.status(500).send(err);
		});
});

module.exports = router;
