const { ObjectId } = require("mongodb");
const mongoose = require("mongoose");
const AnswerSchema = require("./answerModel");
const { server, port, dbTests } = require("./mongoConstants.json");

const testSchema = new mongoose.Schema({
	questions: {
		type: [
			{
				text: { type: String, required: true },
				type: { type: String, required: true },
				answers: { type: [AnswerSchema], required: true },
				correctAnswerID: { type: [Number], required: true },
				selectedAnswerID: { type: [Number], required: false },
			},
		],
		required: true,
		_id: false,
	},
	config: {
		type: {
			name: { type: String, required: true },
			deadline: { type: Number, required: true },
			groups: { type: [String], required: false },
			checkRule: { type: String, required: true },
			duration: { type: Number, required: true },
			creator: {
				type: {
					name: { type: String, required: true },
					email: { type: String, required: true },
				},
				required: true,
			},
		},
		required: true,
	},
});

mongoose.set("useCreateIndex", true);
const conn = mongoose.createConnection(
	`mongodb://${server}:${port}/${dbTests}`,
	{
		newUrlParser: true,
		useUnifiedTopology: true,
	},
);

module.exports = {
	TestModel: conn.model("test", testSchema),
	// AnswerModel: conn.model("answer", testSchema),
};
