const { ObjectId } = require("mongodb");
const mongoose = require("mongoose");
const { server, port, dbGrades } = require("./mongoConstants.json");

const gradeSchema = new mongoose.Schema({
	studentEmail: {
		type: String,
		required: true,
		match: /\S+@edu.hse.ru/,
	},
	studentName: { type: String, required: true },
	grade: { type: Number, required: false },
	testID: { type: ObjectId, required: true },
	selectedAnswers: {
		type: [[Number]],
		required: true,
	},
});

mongoose.set("useCreateIndex", true);
mongoose.connect(`mongodb://${server}:${port}/${dbGrades}`, {
	newUrlParser: true,
});

module.exports = mongoose.model("grade", gradeSchema);
