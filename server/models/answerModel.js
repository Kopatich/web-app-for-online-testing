const mongoose = require("mongoose");

const answerSchema = new mongoose.Schema(
	{
		text: { type: String, required: true },
		ID: { type: Number, required: true },
		type: { type: String, required: true },
	},
	{ _id: false },
);

module.exports = answerSchema;
