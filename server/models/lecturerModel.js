const mongoose = require("mongoose");
const { server, port, dbUsers } = require("./mongoConstants.json");

const userSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
});

mongoose.set("useCreateIndex", true);
const conn = mongoose.createConnection(
	`mongodb://${server}:${port}/${dbUsers}`,
	{
		newUrlParser: true,
		useUnifiedTopology: true,
	},
);

module.exports = conn.model("lecturer", userSchema);
