const { app, BrowserWindow, ipcMain, screen } = require("electron");
const path = require("path");
const url = require("url");
const config = process.env.ELECTRON_INIT_CONFIG
	? require("./browserWindowConfigDev.json")
	: require("./browserWindowConfig.json");

let otherScreens;
let mainWindow;
let timerId;

process.setMaxListeners(11);

function createWindow() {
	mainWindow = new BrowserWindow(config);

	const startUrl =
		process.env.ELECTRON_START_URL ||
		url.format({
			pathname: path.join(__dirname, "../index.html"),
			protocol: "file:",
			slashes: true,
		});

	const localhost = "http://localhost:3000";
	mainWindow.loadURL(localhost);
	mainWindow.on("closed", () => app.emit("window-all-closed"));

	// mainWindow.setMenu(null);
}

ipcMain.on("unlockScreen", (e) => {
	mainWindow.setFullScreen(false);
	mainWindow.unmaximize();
	mainWindow.on("blur", () => {});
	mainWindow.on("focus", () => {});
	mainWindow.setAlwaysOnTop(false);

	mainWindow.getChildWindows()?.forEach((screen) => {
		screen.setFullScreen(false);
		screen.close();
	});

	app?.dock?.show();
});

ipcMain.on("activateScreenLock", (e) => {
	mainWindow.setVisibleOnAllWorkspaces(true, { visibleOnFullScreen: true });

	const displays = screen.getAllDisplays();
	const primaryScreen = screen.getPrimaryDisplay();
	const { x, y } = primaryScreen.bounds;

	app?.dock?.hide();

	mainWindow.setPosition(x, y, true);
	mainWindow.setFullScreen(true);
	mainWindow.setAlwaysOnTop(true, "screen-saver", -100);

	otherScreens = displays.filter((d) => d.id !== primaryScreen.id);
	otherScreens = otherScreens.map((s) => {
		const { x, y, width, height } = s.bounds;
		const window = new BrowserWindow({
			...config,
			x,
			y,
			width: width - 1,
			height,
			alwaysOnTop: false,
			movable: false,
			frame: false,
			parent: mainWindow,
		});
		window.hide();
		return window;
	});

	otherScreens.forEach((window) => {
		window.loadURL("");
		window.showInactive();
		window.setAlwaysOnTop(true, "pop-up-menu", -100);
		window.setFullScreen(true);
	});
});

ipcMain.on("setDeadline", (event, arg) => {
	const duration = arg;
	timerId = setTimeout(() => {
		ipcMain.emit("activateScreenLock");
	}, duration);
});

ipcMain.on("clearDeadline", (e) => {
	clearTimeout(timerId);
	ipcMain.emit("unlockScreen");
});

app.whenReady().then(createWindow);

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") {
		app.quit();
	}
});

app.on("activate", () => {
	if (BrowserWindow.getAllWindows().length === 0) {
		createWindow();
	}
});
